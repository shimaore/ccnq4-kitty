Which fields are stored as List/Array but are actually Set?

    module.exports = new Set [
      'skills'
      'queues'
      'tags'
      'groups'
      'allowed_groups'
      'dates' # calendars[N].dates

      'hotdesk_lines' # kwaoo
    ]
